1
00:00:04,277 --> 00:00:07,445
SUMNER SE ÎNTOARCE

2
00:00:15,032 --> 00:00:19,325
Bună !
Știu că ești surprins, că mă vezi.

3
00:00:20,951 --> 00:00:25,869
Dar mi-am luat tot curajul posibil,
pentru a-mi arăta fața aici.

4
00:00:26,078 --> 00:00:29,746
Fii serios ! Aici apar multe fețe ciudate.

5
00:00:32,706 --> 00:00:35,290
Nu-ți aduci aminte de mine, așa-i ?

6
00:00:35,499 --> 00:00:41,377
Sunt profesorul Sumner Sloan.
Am venit aici acum câteva luni cu Diane Chambers.

7
00:00:41,585 --> 00:00:45,711
Spre rușinea mea, am părăsit-o
aici, ca să mă întorc la fosta soție.

8
00:00:45,878 --> 00:00:51,130
Cu ce te pot servi ?
Un pahar de vin alb.

9
00:00:51,339 --> 00:00:57,092
La o cină la care am fost aseară, un prieten
comun mi-a spus că lucrează aici. E adevărat ?

10
00:00:57,300 --> 00:01:00,260
Nu știu, n-am fost la acea cină.

11
00:01:05,512 --> 00:01:07,513
Gândac nenorocit !

12
00:01:08,471 --> 00:01:13,474
Ești un nenorocit.
Nu meriți să trăiești cu șobolanii de canalizare.

13
00:01:13,683 --> 00:01:18,601
Nu mă pot apăra de asta.
Credeam că vorbește cu mine.

14
00:02:29,465 --> 00:02:32,550
Bună ziua tuturor !
Norm !

15
00:02:32,758 --> 00:02:35,926
Credeam că sunteți la baseball.

16
00:02:36,135 --> 00:02:40,928
Am plecat indignați.
Ne-au cerut să schimbăm scaunele.

17
00:02:41,137 --> 00:02:46,681
De ce v-a cerut să schimbați scaunele ?
Ne-am dat jos cămășile să ne prindă soarele un pic.

18
00:02:46,889 --> 00:02:51,017
Au zis că strălucirea lui Norm a orbit laptele.

19
00:02:51,225 --> 00:02:53,809
Ce pot face dacă am pielea de alabastru ?

20
00:02:54,018 --> 00:02:57,561
E în regulă. Baseballul ar
trebui să fie jucat noaptea.

21
00:03:00,645 --> 00:03:02,646
Nu spuneți nimic.

22
00:03:11,026 --> 00:03:14,193
O, l-am ucis.

23
00:03:15,736 --> 00:03:18,611
Un singur lucru mă poate salva.

24
00:03:21,946 --> 00:03:26,657
Îmi e dor de zilele bune, când
vomitau la vederea celuilalt.

25
00:03:28,908 --> 00:03:31,492
Bună, la toți !
Băieți, ce se întâmplă ?

26
00:03:31,701 --> 00:03:35,411
E cineva care o așteaptă pe
Diane la toaleta bărbaților.

27
00:03:35,619 --> 00:03:40,788
O, Coach, câteodată nu pui
participiul la locul lui.

28
00:03:40,997 --> 00:03:43,914
Da, am dormit pe stomac azi noapte.

29
00:03:45,540 --> 00:03:47,958
Are dreptate, scândură.
Ai musafir.

30
00:03:51,251 --> 00:03:55,295
Diane, te rog, e sensibil cu privire la fața lui.

31
00:03:59,171 --> 00:04:01,839
Ai zis că vii imediat.

32
00:04:02,839 --> 00:04:07,383
Acum un an.
Știi tu, traficul e aglomerat în Boston.

33
00:04:08,383 --> 00:04:11,051
Diane, trebuie să vorbesc cu tine.

34
00:04:11,218 --> 00:04:13,219
Ai dreptul să mă refuzi.

35
00:04:13,427 --> 00:04:17,554
Dar totdeauna ai ascultat
punctul de vedere al altora.

36
00:04:20,347 --> 00:04:23,849
Sam, putem folosi biroul tău câteva minute ?

37
00:04:25,099 --> 00:04:27,100
Desigur.

38
00:04:36,645 --> 00:04:39,564
Nu-ți fă griji, Sam.
Nu sunt îngrijorat.

39
00:04:39,772 --> 00:04:43,690
Nu ai de ce.
Tu și cu Diane vă aveți bine.

40
00:04:43,899 --> 00:04:48,151
Oricine l-ar alege pe cum-îl-cheamă
în defavoarea ta, ar fi nebună.

41
00:04:48,359 --> 00:04:50,611
Crezi că a dezbrăcat-o deja ?

42
00:04:50,818 --> 00:04:52,819
Glumeam !

43
00:04:53,737 --> 00:04:58,989
Înainte să spui ceva, lasă-mă să
zic eu întâi că am uitat de tine.

44
00:04:59,198 --> 00:05:02,032
Așa că sper, că n-ai venit să mă iei înapoi.

45
00:05:02,240 --> 00:05:07,117
Nu am venit.
O,... bine.

46
00:05:07,326 --> 00:05:11,703
Sunt tot cu Barbara, dar tot mai vreau
disperat să te recuceresc ca prietenă, Diane.

47
00:05:11,912 --> 00:05:14,871
Nu pot trăi, știind că cineva mă urăște.

48
00:05:15,079 --> 00:05:20,248
Nu te urăsc, Sumner.
Nu pot trăi, știind că cineva mă crede...

49
00:05:20,457 --> 00:05:24,583
detestabil ? S-ar putea să fii
nevoit să trăiești cu asta.

50
00:05:24,750 --> 00:05:29,544
Doamne ! Chiar și un zâmbet
de-al tău luminează camera.

51
00:05:29,753 --> 00:05:34,213
Sunt lucruri de-ale tale de
care mi-a fost dor și mie.

52
00:05:34,421 --> 00:05:37,088
Sumner, hai să terminăm cu prostiile.

53
00:05:37,255 --> 00:05:42,466
Terminăm cu prostiile ?
Ce ți-au făcut aici ?

54
00:05:42,674 --> 00:05:45,009
Vrei să fii iertat ?

55
00:05:45,218 --> 00:05:49,427
Ești iertat.
Mulțumesc, Diane.

56
00:05:49,636 --> 00:05:51,637
Mai mult decât merit.

57
00:05:53,387 --> 00:05:57,765
Ce ar fi să ne continuăm viețile ?

58
00:05:57,973 --> 00:06:02,642
Trebuie să fug.
Am oră în curând.

59
00:06:05,684 --> 00:06:08,102
Știi la ce m-am gândit ?

60
00:06:09,227 --> 00:06:12,812
La ce ?
Hai să cinăm împreună.

61
00:06:13,021 --> 00:06:16,981
Noi trei. Vreau s-o cunoști pe Barbara.

62
00:06:18,315 --> 00:06:20,775
Nu vreau să mă întâlnesc cu Barbara.

63
00:06:20,983 --> 00:06:25,860
Dacă vrei să înțelegi ce ți-am făcut,
trebuie să o cunoști pe Barbara.

64
00:06:26,944 --> 00:06:28,945
Nu-mi place ideea.

65
00:06:29,153 --> 00:06:32,404
Ar trebui să-ți placă.
Sunteți suflete pereche.

66
00:06:32,613 --> 00:06:34,864
Aveți multe de împărtăși.

67
00:06:35,864 --> 00:06:38,824
Te rog, Diane, fă-o pentru mine.

68
00:06:39,033 --> 00:06:42,493
Ce a fost odată între noi, nu ne va fi luat.

69
00:06:44,243 --> 00:06:46,369
Bine.
Splendid.

70
00:06:49,954 --> 00:06:52,455
Ești cu cineva acum ?

71
00:06:55,040 --> 00:06:57,874
Nu.
N-ai pe cine aduce cu tine ?

72
00:06:59,083 --> 00:07:01,376
Nu. Foarte bine, doar noi trei.

73
00:07:01,584 --> 00:07:03,585
Abia aștept.

74
00:07:04,543 --> 00:07:06,753
Pe curând.

75
00:07:07,795 --> 00:07:11,380
Nu mă înnebunesc după
decoratorul tău, Sam, bătrâne.

76
00:07:15,632 --> 00:07:18,217
E în regulă, e doar un foc de avertizare.

77
00:07:19,258 --> 00:07:23,552
Îți vine să crezi că a venit să-și
ceară scuze pentru tot ce mi-a făcut ?

78
00:07:23,760 --> 00:07:26,345
Vrei să-i trag un pumn ?

79
00:07:26,553 --> 00:07:31,222
Nu, cel mai bine e să lăsăm să
se estompeze în memoria noastră.

80
00:07:32,348 --> 00:07:35,516
Tu ești singurul din lume pentru mine.

81
00:07:35,724 --> 00:07:41,227
Diane, am oră de lectură vineri.
Poți veni sâmbătă ?

82
00:07:44,812 --> 00:07:47,104
Sigur.
Grozav, ne vedem atunci.

83
00:07:48,105 --> 00:07:53,565
Înainte să te enervezi,
se poate explica foarte ușor.

84
00:07:53,774 --> 00:07:56,691
Vrea s-o întâlnesc pe soția lui, Barbara.

85
00:07:56,900 --> 00:08:00,485
E doar o cină nevinovată.
Nu vroiam să-ți faci griji, așa că...

86
00:08:00,693 --> 00:08:02,694
i-am zis o mică minciună.

87
00:08:02,902 --> 00:08:06,695
Încă un lucru, Diane.
Din moment ce nu te vezi cu nimeni,

88
00:08:06,904 --> 00:08:11,240
cunosc un tânăr la departamentul
de filozofie care ți-ar plăcea.

89
00:08:11,448 --> 00:08:15,158
Nu, mulțumesc.
Cum dorești.

90
00:08:18,618 --> 00:08:23,537
Asta e mai greu.
Stai să explic.

91
00:08:23,745 --> 00:08:27,539
Sam, mă asculți ?
M-am săturat să te ascult.

92
00:08:27,746 --> 00:08:31,124
Voi vă certați doar ca să-mi
țineți speranțele vii, nu ?

93
00:08:32,499 --> 00:08:37,918
Nu înțelegi. Nu i-am zis de noi
pentru că nu cred că e treaba lui.

94
00:08:38,085 --> 00:08:42,711
De ce m-ai mințit pe mine ?
Nu te-am mințit.

95
00:08:42,920 --> 00:08:45,587
Doar nu ți-am spus.
E o diferență.

96
00:08:45,754 --> 00:08:48,589
E o diferență prea subtilă, Diane.

97
00:08:54,383 --> 00:08:58,593
Trebuia să-ți spun. Îmi pare rău !
E în regulă.

98
00:09:00,386 --> 00:09:06,222
Nu sunt idiot. Nu i-ai zis de
mine pentru că ți-e rușine cu mine.

99
00:09:06,430 --> 00:09:08,806
Nu mi-e rușine de tine.

100
00:09:11,808 --> 00:09:16,518
Sam, nu înțelegi. Mereu am
fost intimidat de acel om.

101
00:09:16,726 --> 00:09:21,145
Crede că oricine e sub nivelul lui.
Faci o greșeală, te-a taxat imediat.

102
00:09:21,353 --> 00:09:23,354
Încercam să te protejez.

103
00:09:23,563 --> 00:09:28,648
Tu spui că sunt prea prost ca să fiu
asociat ca prieten al tău. Nu e în regulă.

104
00:09:28,857 --> 00:09:33,525
Și încă un lucru. Tu ești
la fel de snoabă cum e și el.

105
00:09:35,484 --> 00:09:37,944
Ai dreptate.

106
00:09:44,697 --> 00:09:50,658
Mă duc la Sumner chiar acum, și îi spun
că tu vei fi perechea mea sâmbătă seara.

107
00:09:52,033 --> 00:09:54,034
Ești amicul meu.

108
00:09:56,119 --> 00:09:58,120
Și mă mândresc cu asta, Sam.

109
00:10:06,123 --> 00:10:08,875
Sammy !
Marchează !

110
00:10:12,876 --> 00:10:14,877
Așa ! Da !

111
00:10:16,003 --> 00:10:19,004
Treabă ca lumea, Sammy.

112
00:10:19,212 --> 00:10:24,423
Te-ai certat ca să-ți
petreci seara cu trei Diane.

113
00:10:25,381 --> 00:10:29,550
Are dreptate. Sumner e un creier mare, Sam.

114
00:10:29,758 --> 00:10:33,802
E un pitbull intelectual, Sammy.
Dacă simte frică, atacă.

115
00:10:34,010 --> 00:10:37,845
Te aduce la o stare de carne bâlbâită
chiar în fața plăcintucii tale.

116
00:10:38,053 --> 00:10:41,555
Urâtă treabă, nu ?

117
00:10:41,763 --> 00:10:43,764
E rău, Sam.

118
00:10:45,140 --> 00:10:48,516
Da, dar nu sunt chiar așa de prost, nu ?

119
00:10:54,686 --> 00:10:58,730
Doamne ! Ce am făcut ?
O să par un idiot.

120
00:10:58,937 --> 00:11:03,898
Sam, va fi bine. Vorbește despre
ceva de care tu știi și ei nu.

121
00:11:04,107 --> 00:11:06,649
De exemplu apartamentul tău.

122
00:11:06,858 --> 00:11:11,193
Coach, nu cred că cineva vrea
să vorbească de apartamentul meu.

123
00:11:11,401 --> 00:11:14,737
Sigur că nu.
Dar vor arăta ca proștii, încercând.

124
00:11:16,778 --> 00:11:20,989
Nu, Diane are dreptate.
O fac de rușine.

125
00:11:21,156 --> 00:11:24,741
Crezi sau nu, am o soluție simplă pentru asta.

126
00:11:24,949 --> 00:11:26,908
E profesor de literatură, nu ?

127
00:11:27,117 --> 00:11:30,243
Da. „Război și pace” e cel
mai mare roman scris vreodată.

128
00:11:30,868 --> 00:11:33,786
Îl citești de acum până sâmbătă.

129
00:11:33,995 --> 00:11:37,747
Arunci câteva comentarii la cină.
Ești ca și salvat.

130
00:11:37,954 --> 00:11:42,498
„Război și pace” e bun ?
Primele 800 de păgâni se citesc mai greu,

131
00:11:42,707 --> 00:11:44,833
dar după ăia merge ca uns.

132
00:11:45,041 --> 00:11:47,917
Am 5 zile la dispoziție.
Cât de mare e cartea ?

133
00:11:48,126 --> 00:11:54,712
L-am livrat la câteva cluburi de
carte și are cam un kilogram și jumătate.

134
00:11:55,588 --> 00:12:00,256
Las-o baltă, Sam, nimeni nu poate
citi o jumătate de kil într-o zi.

135
00:12:00,465 --> 00:12:04,841
Nu, nu, dacă e cea mai bună carte, am s-o citesc.

136
00:12:05,050 --> 00:12:07,092
Mă duc s-o iau acum.
Cum îi spunea ?

137
00:12:08,801 --> 00:12:12,511
„Război și pace”.
Trebuie s-o notezi ?

138
00:12:13,636 --> 00:12:15,637
Și să greșești ?

139
00:12:18,639 --> 00:12:21,307
Apartamentul, Sam.
Apartamentul.

140
00:12:42,217 --> 00:12:46,094
Intră !
Ce mai faci, Sammy ?

141
00:12:46,303 --> 00:12:49,888
Voi erați ?
Credeam că e Diane.

142
00:12:50,096 --> 00:12:52,222
Aprindeți lumina !

143
00:12:54,140 --> 00:12:59,850
N-am mai fost aici.
Select, Sam.

144
00:13:01,726 --> 00:13:04,102
Știi de unde pot lua una ca asta ?

145
00:13:04,311 --> 00:13:06,312
Se apropie ziua mamei.

146
00:13:07,520 --> 00:13:11,356
Vreți să mă lăsați câteva minute ?
Aproape am terminat.

147
00:13:11,564 --> 00:13:14,690
Cât e ceasul ?
Câte beri am băut ?

148
00:13:14,899 --> 00:13:16,900
11.
8.05.

149
00:13:18,984 --> 00:13:21,234
Doamne, sosește dintr-un moment în altul.

150
00:13:22,235 --> 00:13:24,570
Doamne !
Cafea rece.

151
00:13:24,778 --> 00:13:27,946
Sunt treaz de 5 nopți.

152
00:13:30,030 --> 00:13:32,365
Mă simt grozav.
Chiar mă simt.

153
00:13:32,573 --> 00:13:35,241
De ce m-am ridicat ?

154
00:13:36,909 --> 00:13:39,910
Să-ți aranjezi chiloții ?
Nu.

155
00:13:40,951 --> 00:13:46,496
Hei, Sam, a venit Cap de Brânză.
Mulțumesc Carla. Unde e Diane ?

156
00:13:46,704 --> 00:13:51,664
S-a dus la baie cu un tub de
rimel și un cuțit de chituit.

157
00:13:53,373 --> 00:13:55,750
Noi am ieșit, Sammy.
Pa, Sammy.

158
00:13:55,958 --> 00:14:00,919
Stați așa ! N-are vreunul din voi două baterii B ?

159
00:14:01,127 --> 00:14:03,670
Am una.

160
00:14:05,587 --> 00:14:07,296
Nu.

161
00:14:10,673 --> 00:14:13,007
Sumner.
- Ah, Diane.

162
00:14:15,300 --> 00:14:18,176
Unde-i Barbara.
N-a mai ajuns. E bolnavă.

163
00:14:18,384 --> 00:14:23,137
Atunci o s-o facem altă dată.
Nici nu vreau să aud !

164
00:14:23,345 --> 00:14:27,680
Vom fi mai săraci pentru că va lipsi,
dar mai bogați când vine nota de plată.

165
00:14:30,640 --> 00:14:33,308
Dar ideea era să o întâlnesc.

166
00:14:33,517 --> 00:14:38,185
Ea a insistat ca boala ei să nu ne strice seara.
Nu e magnific ?

167
00:14:38,393 --> 00:14:40,394
Așa am auzit.

168
00:14:41,603 --> 00:14:46,147
Sam, dacă vrei să te distrezi,
mai bine iei un yo-yo cu tine.

169
00:14:47,231 --> 00:14:49,232
Băieți, e gata, am terminat-o.

170
00:14:49,440 --> 00:14:51,441
Sammy, pantofi, pantofi...

171
00:14:55,442 --> 00:14:57,443
Mulțumesc.
Vine acum.

172
00:14:57,652 --> 00:14:59,653
- Sam ?

173
00:15:00,986 --> 00:15:02,987
Nu te-ai bărbierit ?

174
00:15:03,196 --> 00:15:06,489
Nu, nu, am avut de
zgâriat în locuri noi.

175
00:15:08,031 --> 00:15:12,534
Unde e femeia ta ?
Barbara e bolnavă.

176
00:15:12,742 --> 00:15:16,701
N-a mai ajuns.
Dar spiritul ei va fi cu noi.

177
00:15:16,910 --> 00:15:20,537
Avem rezervare la 8:30 la Maison Robert.

178
00:15:20,745 --> 00:15:24,956
Minunat.
Aștept de mult seara asta.

179
00:15:25,164 --> 00:15:29,666
Aștept de mult să stau jos și să vorbesc
cu tipul ăsta despre „Război și pace”.

180
00:15:30,750 --> 00:15:34,252
„Război și pace”?
Da. Bună carte. Clasică.

181
00:15:34,459 --> 00:15:39,254
Am avut seminarul despre Tolstoi 6 ani.
Am jurat să nu mai discut despre el niciodată.

182
00:15:39,462 --> 00:15:43,130
E cel mai analizat roman scris vreodată.

183
00:15:51,676 --> 00:15:53,677
Mulțumesc.

184
00:15:59,512 --> 00:16:02,722
Se pare că se gătește ceva bun la Melville.

185
00:16:02,930 --> 00:16:05,056
Da, mă întreb, ce o fi, Norm ?

186
00:16:05,265 --> 00:16:09,058
Ar fi supă cremă de măcriș, Coach.

187
00:16:09,267 --> 00:16:13,643
Urmat de midii coapte
ușor și cu unt cu tarhon,

188
00:16:13,851 --> 00:16:18,354
continuat de un tort de zmeură fină.

189
00:16:20,105 --> 00:16:24,356
Delicatesele fine sunt una
dintre marile plăceri ale vieții.

190
00:16:24,564 --> 00:16:26,565
Absolut !

191
00:16:28,316 --> 00:16:31,442
Coach, care-i data de expirare la astea ?

192
00:16:38,946 --> 00:16:40,947
Ieri.

193
00:16:42,281 --> 00:16:44,282
Ar trebui să ne grăbim.

194
00:16:56,495 --> 00:16:59,163
Sam, ce zici de niște băuturi, să spele cina ?

195
00:17:01,914 --> 00:17:05,332
A, engleză ! Îmi pare rău.
Ce ai întrebat ?

196
00:17:05,541 --> 00:17:08,209
Băuturi ?
Bine. Ce doriți ?

197
00:17:08,418 --> 00:17:11,544
Un cognac.
Deux.

198
00:17:18,005 --> 00:17:20,923
Cum a fost cina, Sammy ?
Bună, Sam.

199
00:17:21,131 --> 00:17:23,132
Grozav.
Cină grozavă.

200
00:17:23,340 --> 00:17:27,050
Mâncarea a fost franțuzească,
conversația a fost în greacă.

201
00:17:27,259 --> 00:17:31,678
Toată seară am dat din cap ca idiotul.

202
00:17:31,885 --> 00:17:33,886
Înțeleg.

203
00:17:36,722 --> 00:17:40,889
Hei, Sammy ! Ce ar fi să arunci trampa aia afară ?

204
00:17:41,098 --> 00:17:46,183
Nu, Diane ar fi furioasă.
Eu vorbeam de Diane.

205
00:17:46,392 --> 00:17:49,643
Nu i-aș lăsa împreună
pe cei doi, în locul tău.

206
00:17:51,769 --> 00:17:55,438
Nu am mai văzut-o pe Diane
distrându-se de mult așa.

207
00:17:55,646 --> 00:17:57,647
Mulțumesc, prieteni !

208
00:17:58,814 --> 00:18:02,649
Singura dovadă verificabilă
a existenței supreme fiind...

209
00:18:02,858 --> 00:18:06,151
Despre ce vorbim ?
Vorbim despre Dumnezeu.

210
00:18:06,359 --> 00:18:08,819
Singura dovadă verificabilă
a existenței supreme...

211
00:18:09,027 --> 00:18:11,111
Ce-i cu Dumnezeu ?

212
00:18:12,695 --> 00:18:16,739
E destul de încurcat.
Îmi place încurcat. Bagă mare !

213
00:18:16,905 --> 00:18:20,657
Sumner tocmai spunea că misticii
antici îl percepeau pe Dumnezeu

214
00:18:20,866 --> 00:18:24,033
fără a-l asocia cu dovezi palpabile.

215
00:18:27,160 --> 00:18:29,828
Știi ce zic eu în privința asta ?
Ce zici, Sam ?

216
00:18:37,123 --> 00:18:40,249
Care-i problema ta ?
N-am nici o... Ba da, am o problemă.

217
00:18:40,458 --> 00:18:45,626
Problema e că nu am luat parte la
nici o conversație în seara asta.

218
00:18:45,835 --> 00:18:49,712
Am încercat de câteva ori să te
includ în conversația noastră.

219
00:18:49,920 --> 00:18:55,173
Nu cred că „Furculiță greșită !” și „Nu
scuipa aia !” e conversația pe care o voiam.

220
00:18:58,048 --> 00:19:04,301
Nu ești corect. Ți-am cerut opinia
de câteva ori și nu ne-ai dat-o ?

221
00:19:04,509 --> 00:19:08,470
Știi de ce ?
Pentru că nu am avut vreuna.

222
00:19:08,678 --> 00:19:10,679
Ce zici de asta ?

223
00:19:10,888 --> 00:19:14,598
Sau poate aveam, dar nu
vroiam să o irosesc pe voi.

224
00:19:16,515 --> 00:19:20,391
Ce încerci să spui ?
Nu încerc să spun nimic.

225
00:19:20,600 --> 00:19:23,477
O spun. Noi doi suntem o glumă, Diane.

226
00:19:23,685 --> 00:19:26,686
Tu cu Sumner sunteți tot o
glumă, dar măcar e aceeași glumă.

227
00:19:26,895 --> 00:19:30,146
Sunt prost în multe privințe,
dar știu și eu câteva lucruri.

228
00:19:30,354 --> 00:19:34,356
Știu când doi oameni nu se
potrivesc și noi doi nu ne potrivim.

229
00:19:34,564 --> 00:19:36,940
Sam, stai jos.
Creezi agitație.

230
00:19:37,149 --> 00:19:40,734
Haideți ! Știu ce s-a petrecut toată seara.

231
00:19:40,942 --> 00:19:44,194
Chestia că Barbara e
bolnavă e numai o prostie.

232
00:19:44,402 --> 00:19:49,487
Ai aranjat totul ca să-ți recapeți marele
fan înapoi. Ai reușit ! Ține-o tot așa, băiete !

233
00:19:50,696 --> 00:19:54,740
Sam, te rog !
Acum văd totul foarte clar.

234
00:19:54,948 --> 00:19:59,909
Ai fost o minge recuperată. Erai
doar aruncată între mine și doctori.

235
00:20:00,117 --> 00:20:04,911
A venit după tine. Du-te cu el.
Îți mai spun ceva.

236
00:20:05,119 --> 00:20:08,954
Când o să mai citesc „Război și pace” încă o dată
în cinci zile ca să impresionez o femeiușcă,

237
00:20:09,163 --> 00:20:11,748
va fi o zi rece în Minsk.

238
00:20:25,045 --> 00:20:30,547
Trebuie să-mi cer scuze în locul lui
Sam pentru că a făcut acuzații nefondate.

239
00:20:31,756 --> 00:20:35,007
E mai isteț decât am crezut.

240
00:20:37,967 --> 00:20:39,968
Ce ai spus ?

241
00:20:40,177 --> 00:20:43,261
Am vrut să văd, dacă a mai rămas o scânteie

242
00:20:43,470 --> 00:20:47,055
din acel foc măreț ce a
traversat cerul ca un meteor.

243
00:20:49,097 --> 00:20:52,515
Barbara și cu mine am luat-o pe
căi separate de câteva săptămâni.

244
00:20:54,350 --> 00:20:57,059
Sam avea dreptate.
Lasă-l pe Sam.

245
00:20:57,267 --> 00:21:01,061
Nu e evident după seara asta
că noi doi avem ceva special ?

246
00:21:01,269 --> 00:21:03,978
Diane, poate nu sunt eu perfect.

247
00:21:04,187 --> 00:21:06,938
Totuși s-ar putea să fiu.

248
00:21:07,146 --> 00:21:13,733
Întrebarea e dacă poți să spui sincer că
aparții mai mult lumii lui decât lumii mele ?

249
00:21:29,323 --> 00:21:33,699
Ai venit să-mi spui la revedere
sau cum spun francezii: ta-ta ?

250
00:21:36,993 --> 00:21:41,411
Nu, am venit să-ți
spun că Sumner a plecat.

251
00:21:43,371 --> 00:21:46,789
Da ?
Carla l-a aruncat afară.

252
00:21:48,539 --> 00:21:51,374
Nu-mi spune că m-ai ales
pe mine în locul lui.

253
00:21:51,582 --> 00:21:54,042
Am dat cu banul.

254
00:21:54,250 --> 00:21:57,960
Ai lăsat un ban să hotărască ?
Da.

255
00:21:58,169 --> 00:22:01,712
I-am zis lui Sumner că dacă iese cap, vin la tine,

256
00:22:01,921 --> 00:22:04,838
dacă iese pajură, alerg la tine.

257
00:22:07,340 --> 00:22:11,883
Ești nebună, știi ?
Adică individul e genial.

258
00:22:12,092 --> 00:22:16,343
E șarmant, sofisticat.
Da ? Dacă te grăbești, poți să-l iei.

259
00:22:23,222 --> 00:22:27,681
De ce m-ai ales pe mine ?
Tu ai citit „Război și pace”.

260
00:22:28,765 --> 00:22:31,809
Și el.
Tu ai făcut-o pentru mine.

261
00:22:33,434 --> 00:22:35,602
Cred că a fost mai greu pentru tine.

262
00:22:38,019 --> 00:22:40,020
Spune-i inspirație.

263
00:22:41,813 --> 00:22:44,272
Nu e ca o zi pe plajă.

264
00:22:49,900 --> 00:22:54,402
Un lucru e mai romantic decât să
citești „Război și pace” pentru mine.

265
00:22:54,610 --> 00:22:57,986
Care ?
Să-mi citești mie din „Război și pace”.

266
00:22:58,195 --> 00:23:00,196
Da ?

267
00:23:00,404 --> 00:23:04,156
Se întâmplă să am o copie aici.

268
00:23:04,364 --> 00:23:08,282
Stăm aici jos și eu am să-ți citesc.

269
00:23:09,534 --> 00:23:11,534
Ce drăguț !

270
00:23:11,742 --> 00:23:13,743
Hai că încep !

271
00:23:14,786 --> 00:23:17,245
Hai să vedem filmul.

272
00:23:18,412 --> 00:23:24,414
Au făcut filmul ? Unde e Cliff ?
Îl omor !

273
00:24:00,889 --> 00:24:04,148
Traducerea: tinikabv
