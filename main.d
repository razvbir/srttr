import std.stdio : stderr;
import std.file : exists, isFile, read, write;
import std.encoding : Latin1String, transcode;
import std.string : tr, replace, endsWith;

int main(string[] argv) {

    if (argv.length != 2) {
        stderr.writeln("A file must be provided");
        return 1;
    }

    string fileName = argv[1];
    if (! (exists(fileName) && isFile(fileName))) {
        stderr.writeln("The provided file does not exists");
        return 1;
    }

    if (! fileName.endsWith(".srt")) {
        stderr.writeln("The provided file is not a subtitle");
        return 1;
    }
    
    Latin1String latin1Text = cast(Latin1String) read(fileName);
    string utf8Text;
    
    transcode(latin1Text, utf8Text);
    utf8Text = utf8Text.tr("ºªãÃþÞ", "șȘăĂțȚ„”");
    
    write(fileName.replace(".srt", ".mod.srt"), utf8Text);

    return 0;
}
