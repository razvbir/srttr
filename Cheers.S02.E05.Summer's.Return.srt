1
00:00:04,277 --> 00:00:07,445
SUMNER SE �NTOARCE

2
00:00:15,032 --> 00:00:19,325
Bun� !
�tiu c� e�ti surprins, c� m� vezi.

3
00:00:20,951 --> 00:00:25,869
Dar mi-am luat tot curajul posibil,
pentru a-mi ar�ta fa�a aici.

4
00:00:26,078 --> 00:00:29,746
Fii serios ! Aici apar multe fe�e ciudate.

5
00:00:32,706 --> 00:00:35,290
Nu-�i aduci aminte de mine, a�a-i ?

6
00:00:35,499 --> 00:00:41,377
Sunt profesorul Sumner Sloan.
Am venit aici acum c�teva luni cu Diane Chambers.

7
00:00:41,585 --> 00:00:45,711
Spre ru�inea mea, am p�r�sit-o
aici, ca s� m� �ntorc la fosta so�ie.

8
00:00:45,878 --> 00:00:51,130
Cu ce te pot servi ?
Un pahar de vin alb.

9
00:00:51,339 --> 00:00:57,092
La o cin� la care am fost asear�, un prieten
comun mi-a spus c� lucreaz� aici. E adev�rat ?

10
00:00:57,300 --> 00:01:00,260
Nu �tiu, n-am fost la acea cin�.

11
00:01:05,512 --> 00:01:07,513
G�ndac nenorocit !

12
00:01:08,471 --> 00:01:13,474
E�ti un nenorocit.
Nu meri�i s� tr�ie�ti cu �obolanii de canalizare.

13
00:01:13,683 --> 00:01:18,601
Nu m� pot ap�ra de asta.
Credeam c� vorbe�te cu mine.

14
00:02:29,465 --> 00:02:32,550
Bun� ziua tuturor !
Norm !

15
00:02:32,758 --> 00:02:35,926
Credeam c� sunte�i la baseball.

16
00:02:36,135 --> 00:02:40,928
Am plecat indigna�i.
Ne-au cerut s� schimb�m scaunele.

17
00:02:41,137 --> 00:02:46,681
De ce v-a cerut s� schimba�i scaunele ?
Ne-am dat jos c�m�ile s� ne prind� soarele un pic.

18
00:02:46,889 --> 00:02:51,017
Au zis c� str�lucirea lui Norm a orbit laptele.

19
00:02:51,225 --> 00:02:53,809
Ce pot face dac� am pielea de alabastru ?

20
00:02:54,018 --> 00:02:57,561
E �n regul�. Baseballul ar
trebui s� fie jucat noaptea.

21
00:03:00,645 --> 00:03:02,646
Nu spune�i nimic.

22
00:03:11,026 --> 00:03:14,193
O, l-am ucis.

23
00:03:15,736 --> 00:03:18,611
Un singur lucru m� poate salva.

24
00:03:21,946 --> 00:03:26,657
�mi e dor de zilele bune, c�nd
vomitau la vederea celuilalt.

25
00:03:28,908 --> 00:03:31,492
Bun�, la to�i !
B�ie�i, ce se �nt�mpl� ?

26
00:03:31,701 --> 00:03:35,411
E cineva care o a�teapt� pe
Diane la toaleta b�rba�ilor.

27
00:03:35,619 --> 00:03:40,788
O, Coach, c�teodat� nu pui
participiul la locul lui.

28
00:03:40,997 --> 00:03:43,914
Da, am dormit pe stomac azi noapte.

29
00:03:45,540 --> 00:03:47,958
Are dreptate, sc�ndur�.
Ai musafir.

30
00:03:51,251 --> 00:03:55,295
Diane, te rog, e sensibil cu privire la fa�a lui.

31
00:03:59,171 --> 00:04:01,839
Ai zis c� vii imediat.

32
00:04:02,839 --> 00:04:07,383
Acum un an.
�tii tu, traficul e aglomerat �n Boston.

33
00:04:08,383 --> 00:04:11,051
Diane, trebuie s� vorbesc cu tine.

34
00:04:11,218 --> 00:04:13,219
Ai dreptul s� m� refuzi.

35
00:04:13,427 --> 00:04:17,554
Dar totdeauna ai ascultat
punctul de vedere al altora.

36
00:04:20,347 --> 00:04:23,849
Sam, putem folosi biroul t�u c�teva minute ?

37
00:04:25,099 --> 00:04:27,100
Desigur.

38
00:04:36,645 --> 00:04:39,564
Nu-�i f� griji, Sam.
Nu sunt �ngrijorat.

39
00:04:39,772 --> 00:04:43,690
Nu ai de ce.
Tu �i cu Diane v� ave�i bine.

40
00:04:43,899 --> 00:04:48,151
Oricine l-ar alege pe cum-�l-cheam�
�n defavoarea ta, ar fi nebun�.

41
00:04:48,359 --> 00:04:50,611
Crezi c� a dezbr�cat-o deja ?

42
00:04:50,818 --> 00:04:52,819
Glumeam !

43
00:04:53,737 --> 00:04:58,989
�nainte s� spui ceva, las�-m� s�
zic eu �nt�i c� am uitat de tine.

44
00:04:59,198 --> 00:05:02,032
A�a c� sper, c� n-ai venit s� m� iei �napoi.

45
00:05:02,240 --> 00:05:07,117
Nu am venit.
O,... bine.

46
00:05:07,326 --> 00:05:11,703
Sunt tot cu Barbara, dar tot mai vreau
disperat s� te recuceresc ca prieten�, Diane.

47
00:05:11,912 --> 00:05:14,871
Nu pot tr�i, �tiind c� cineva m� ur�te.

48
00:05:15,079 --> 00:05:20,248
Nu te ur�sc, Sumner.
Nu pot tr�i, �tiind c� cineva m� crede...

49
00:05:20,457 --> 00:05:24,583
detestabil ? S-ar putea s� fii
nevoit s� tr�ie�ti cu asta.

50
00:05:24,750 --> 00:05:29,544
Doamne ! Chiar �i un z�mbet
de-al t�u lumineaz� camera.

51
00:05:29,753 --> 00:05:34,213
Sunt lucruri de-ale tale de
care mi-a fost dor �i mie.

52
00:05:34,421 --> 00:05:37,088
Sumner, hai s� termin�m cu prostiile.

53
00:05:37,255 --> 00:05:42,466
Termin�m cu prostiile ?
Ce �i-au f�cut aici ?

54
00:05:42,674 --> 00:05:45,009
Vrei s� fii iertat ?

55
00:05:45,218 --> 00:05:49,427
E�ti iertat.
Mul�umesc, Diane.

56
00:05:49,636 --> 00:05:51,637
Mai mult dec�t merit.

57
00:05:53,387 --> 00:05:57,765
Ce ar fi s� ne continu�m vie�ile ?

58
00:05:57,973 --> 00:06:02,642
Trebuie s� fug.
Am or� �n cur�nd.

59
00:06:05,684 --> 00:06:08,102
�tii la ce m-am g�ndit ?

60
00:06:09,227 --> 00:06:12,812
La ce ?
Hai s� cin�m �mpreun�.

61
00:06:13,021 --> 00:06:16,981
Noi trei. Vreau s-o cuno�ti pe Barbara.

62
00:06:18,315 --> 00:06:20,775
Nu vreau s� m� �nt�lnesc cu Barbara.

63
00:06:20,983 --> 00:06:25,860
Dac� vrei s� �n�elegi ce �i-am f�cut,
trebuie s� o cuno�ti pe Barbara.

64
00:06:26,944 --> 00:06:28,945
Nu-mi place ideea.

65
00:06:29,153 --> 00:06:32,404
Ar trebui s�-�i plac�.
Sunte�i suflete pereche.

66
00:06:32,613 --> 00:06:34,864
Ave�i multe de �mp�rt�i.

67
00:06:35,864 --> 00:06:38,824
Te rog, Diane, f�-o pentru mine.

68
00:06:39,033 --> 00:06:42,493
Ce a fost odat� �ntre noi, nu ne va fi luat.

69
00:06:44,243 --> 00:06:46,369
Bine.
Splendid.

70
00:06:49,954 --> 00:06:52,455
E�ti cu cineva acum ?

71
00:06:55,040 --> 00:06:57,874
Nu.
N-ai pe cine aduce cu tine ?

72
00:06:59,083 --> 00:07:01,376
Nu. Foarte bine, doar noi trei.

73
00:07:01,584 --> 00:07:03,585
Abia a�tept.

74
00:07:04,543 --> 00:07:06,753
Pe cur�nd.

75
00:07:07,795 --> 00:07:11,380
Nu m� �nnebunesc dup�
decoratorul t�u, Sam, b�tr�ne.

76
00:07:15,632 --> 00:07:18,217
E �n regul�, e doar un foc de avertizare.

77
00:07:19,258 --> 00:07:23,552
��i vine s� crezi c� a venit s�-�i
cear� scuze pentru tot ce mi-a f�cut ?

78
00:07:23,760 --> 00:07:26,345
Vrei s�-i trag un pumn ?

79
00:07:26,553 --> 00:07:31,222
Nu, cel mai bine e s� l�s�m s�
se estompeze �n memoria noastr�.

80
00:07:32,348 --> 00:07:35,516
Tu e�ti singurul din lume pentru mine.

81
00:07:35,724 --> 00:07:41,227
Diane, am or� de lectur� vineri.
Po�i veni s�mb�t� ?

82
00:07:44,812 --> 00:07:47,104
Sigur.
Grozav, ne vedem atunci.

83
00:07:48,105 --> 00:07:53,565
�nainte s� te enervezi,
se poate explica foarte u�or.

84
00:07:53,774 --> 00:07:56,691
Vrea s-o �nt�lnesc pe so�ia lui, Barbara.

85
00:07:56,900 --> 00:08:00,485
E doar o cin� nevinovat�.
Nu vroiam s�-�i faci griji, a�a c�...

86
00:08:00,693 --> 00:08:02,694
i-am zis o mic� minciun�.

87
00:08:02,902 --> 00:08:06,695
�nc� un lucru, Diane.
Din moment ce nu te vezi cu nimeni,

88
00:08:06,904 --> 00:08:11,240
cunosc un t�n�r la departamentul
de filozofie care �i-ar pl�cea.

89
00:08:11,448 --> 00:08:15,158
Nu, mul�umesc.
Cum dore�ti.

90
00:08:18,618 --> 00:08:23,537
Asta e mai greu.
Stai s� explic.

91
00:08:23,745 --> 00:08:27,539
Sam, m� ascul�i ?
M-am s�turat s� te ascult.

92
00:08:27,746 --> 00:08:31,124
Voi v� certa�i doar ca s�-mi
�ine�i speran�ele vii, nu ?

93
00:08:32,499 --> 00:08:37,918
Nu �n�elegi. Nu i-am zis de noi
pentru c� nu cred c� e treaba lui.

94
00:08:38,085 --> 00:08:42,711
De ce m-ai min�it pe mine ?
Nu te-am min�it.

95
00:08:42,920 --> 00:08:45,587
Doar nu �i-am spus.
E o diferen��.

96
00:08:45,754 --> 00:08:48,589
E o diferen�� prea subtil�, Diane.

97
00:08:54,383 --> 00:08:58,593
Trebuia s�-�i spun. �mi pare r�u !
E �n regul�.

98
00:09:00,386 --> 00:09:06,222
Nu sunt idiot. Nu i-ai zis de
mine pentru c� �i-e ru�ine cu mine.

99
00:09:06,430 --> 00:09:08,806
Nu mi-e ru�ine de tine.

100
00:09:11,808 --> 00:09:16,518
Sam, nu �n�elegi. Mereu am
fost intimidat de acel om.

101
00:09:16,726 --> 00:09:21,145
Crede c� oricine e sub nivelul lui.
Faci o gre�eal�, te-a taxat imediat.

102
00:09:21,353 --> 00:09:23,354
�ncercam s� te protejez.

103
00:09:23,563 --> 00:09:28,648
Tu spui c� sunt prea prost ca s� fiu
asociat ca prieten al t�u. Nu e �n regul�.

104
00:09:28,857 --> 00:09:33,525
�i �nc� un lucru. Tu e�ti
la fel de snoab� cum e �i el.

105
00:09:35,484 --> 00:09:37,944
Ai dreptate.

106
00:09:44,697 --> 00:09:50,658
M� duc la Sumner chiar acum, �i �i spun
c� tu vei fi perechea mea s�mb�t� seara.

107
00:09:52,033 --> 00:09:54,034
E�ti amicul meu.

108
00:09:56,119 --> 00:09:58,120
�i m� m�ndresc cu asta, Sam.

109
00:10:06,123 --> 00:10:08,875
Sammy !
Marcheaz� !

110
00:10:12,876 --> 00:10:14,877
A�a ! Da !

111
00:10:16,003 --> 00:10:19,004
Treab� ca lumea, Sammy.

112
00:10:19,212 --> 00:10:24,423
Te-ai certat ca s�-�i
petreci seara cu trei Diane.

113
00:10:25,381 --> 00:10:29,550
Are dreptate. Sumner e un creier mare, Sam.

114
00:10:29,758 --> 00:10:33,802
E un pitbull intelectual, Sammy.
Dac� simte fric�, atac�.

115
00:10:34,010 --> 00:10:37,845
Te aduce la o stare de carne b�lb�it�
chiar �n fa�a pl�cintucii tale.

116
00:10:38,053 --> 00:10:41,555
Ur�t� treab�, nu ?

117
00:10:41,763 --> 00:10:43,764
E r�u, Sam.

118
00:10:45,140 --> 00:10:48,516
Da, dar nu sunt chiar a�a de prost, nu ?

119
00:10:54,686 --> 00:10:58,730
Doamne ! Ce am f�cut ?
O s� par un idiot.

120
00:10:58,937 --> 00:11:03,898
Sam, va fi bine. Vorbe�te despre
ceva de care tu �tii �i ei nu.

121
00:11:04,107 --> 00:11:06,649
De exemplu apartamentul t�u.

122
00:11:06,858 --> 00:11:11,193
Coach, nu cred c� cineva vrea
s� vorbeasc� de apartamentul meu.

123
00:11:11,401 --> 00:11:14,737
Sigur c� nu.
Dar vor ar�ta ca pro�tii, �ncerc�nd.

124
00:11:16,778 --> 00:11:20,989
Nu, Diane are dreptate.
O fac de ru�ine.

125
00:11:21,156 --> 00:11:24,741
Crezi sau nu, am o solu�ie simpl� pentru asta.

126
00:11:24,949 --> 00:11:26,908
E profesor de literatur�, nu ?

127
00:11:27,117 --> 00:11:30,243
Da. �R�zboi �i pace� e cel
mai mare roman scris vreodat�.

128
00:11:30,868 --> 00:11:33,786
�l cite�ti de acum p�n� s�mb�t�.

129
00:11:33,995 --> 00:11:37,747
Arunci c�teva comentarii la cin�.
E�ti ca �i salvat.

130
00:11:37,954 --> 00:11:42,498
�R�zboi �i pace� e bun ?
Primele 800 de p�g�ni se citesc mai greu,

131
00:11:42,707 --> 00:11:44,833
dar dup� �ia merge ca uns.

132
00:11:45,041 --> 00:11:47,917
Am 5 zile la dispozi�ie.
C�t de mare e cartea ?

133
00:11:48,126 --> 00:11:54,712
L-am livrat la c�teva cluburi de
carte �i are cam un kilogram �i jum�tate.

134
00:11:55,588 --> 00:12:00,256
Las-o balt�, Sam, nimeni nu poate
citi o jum�tate de kil �ntr-o zi.

135
00:12:00,465 --> 00:12:04,841
Nu, nu, dac� e cea mai bun� carte, am s-o citesc.

136
00:12:05,050 --> 00:12:07,092
M� duc s-o iau acum.
Cum �i spunea ?

137
00:12:08,801 --> 00:12:12,511
�R�zboi �i pace�.
Trebuie s-o notezi ?

138
00:12:13,636 --> 00:12:15,637
�i s� gre�e�ti ?

139
00:12:18,639 --> 00:12:21,307
Apartamentul, Sam.
Apartamentul.

140
00:12:42,217 --> 00:12:46,094
Intr� !
Ce mai faci, Sammy ?

141
00:12:46,303 --> 00:12:49,888
Voi era�i ?
Credeam c� e Diane.

142
00:12:50,096 --> 00:12:52,222
Aprinde�i lumina !

143
00:12:54,140 --> 00:12:59,850
N-am mai fost aici.
Select, Sam.

144
00:13:01,726 --> 00:13:04,102
�tii de unde pot lua una ca asta ?

145
00:13:04,311 --> 00:13:06,312
Se apropie ziua mamei.

146
00:13:07,520 --> 00:13:11,356
Vre�i s� m� l�sa�i c�teva minute ?
Aproape am terminat.

147
00:13:11,564 --> 00:13:14,690
C�t e ceasul ?
C�te beri am b�ut ?

148
00:13:14,899 --> 00:13:16,900
11.
8.05.

149
00:13:18,984 --> 00:13:21,234
Doamne, sose�te dintr-un moment �n altul.

150
00:13:22,235 --> 00:13:24,570
Doamne !
Cafea rece.

151
00:13:24,778 --> 00:13:27,946
Sunt treaz de 5 nop�i.

152
00:13:30,030 --> 00:13:32,365
M� simt grozav.
Chiar m� simt.

153
00:13:32,573 --> 00:13:35,241
De ce m-am ridicat ?

154
00:13:36,909 --> 00:13:39,910
S�-�i aranjezi chilo�ii ?
Nu.

155
00:13:40,951 --> 00:13:46,496
Hei, Sam, a venit Cap de Br�nz�.
Mul�umesc Carla. Unde e Diane ?

156
00:13:46,704 --> 00:13:51,664
S-a dus la baie cu un tub de
rimel �i un cu�it de chituit.

157
00:13:53,373 --> 00:13:55,750
Noi am ie�it, Sammy.
Pa, Sammy.

158
00:13:55,958 --> 00:14:00,919
Sta�i a�a ! N-are vreunul din voi dou� baterii B ?

159
00:14:01,127 --> 00:14:03,670
Am una.

160
00:14:05,587 --> 00:14:07,296
Nu.

161
00:14:10,673 --> 00:14:13,007
Sumner.
- Ah, Diane.

162
00:14:15,300 --> 00:14:18,176
Unde-i Barbara.
N-a mai ajuns. E bolnav�.

163
00:14:18,384 --> 00:14:23,137
Atunci o s-o facem alt� dat�.
Nici nu vreau s� aud !

164
00:14:23,345 --> 00:14:27,680
Vom fi mai s�raci pentru c� va lipsi,
dar mai boga�i c�nd vine nota de plat�.

165
00:14:30,640 --> 00:14:33,308
Dar ideea era s� o �nt�lnesc.

166
00:14:33,517 --> 00:14:38,185
Ea a insistat ca boala ei s� nu ne strice seara.
Nu e magnific ?

167
00:14:38,393 --> 00:14:40,394
A�a am auzit.

168
00:14:41,603 --> 00:14:46,147
Sam, dac� vrei s� te distrezi,
mai bine iei un yo-yo cu tine.

169
00:14:47,231 --> 00:14:49,232
B�ie�i, e gata, am terminat-o.

170
00:14:49,440 --> 00:14:51,441
Sammy, pantofi, pantofi...

171
00:14:55,442 --> 00:14:57,443
Mul�umesc.
Vine acum.

172
00:14:57,652 --> 00:14:59,653
- Sam ?

173
00:15:00,986 --> 00:15:02,987
Nu te-ai b�rbierit ?

174
00:15:03,196 --> 00:15:06,489
Nu, nu, am avut de
zg�riat �n locuri noi.

175
00:15:08,031 --> 00:15:12,534
Unde e femeia ta ?
Barbara e bolnav�.

176
00:15:12,742 --> 00:15:16,701
N-a mai ajuns.
Dar spiritul ei va fi cu noi.

177
00:15:16,910 --> 00:15:20,537
Avem rezervare la 8:30 la Maison Robert.

178
00:15:20,745 --> 00:15:24,956
Minunat.
A�tept de mult seara asta.

179
00:15:25,164 --> 00:15:29,666
A�tept de mult s� stau jos �i s� vorbesc
cu tipul �sta despre �R�zboi �i pace�.

180
00:15:30,750 --> 00:15:34,252
�R�zboi �i pace�?
Da. Bun� carte. Clasic�.

181
00:15:34,459 --> 00:15:39,254
Am avut seminarul despre Tolstoi 6 ani.
Am jurat s� nu mai discut despre el niciodat�.

182
00:15:39,462 --> 00:15:43,130
E cel mai analizat roman scris vreodat�.

183
00:15:51,676 --> 00:15:53,677
Mul�umesc.

184
00:15:59,512 --> 00:16:02,722
Se pare c� se g�te�te ceva bun la Melville.

185
00:16:02,930 --> 00:16:05,056
Da, m� �ntreb, ce o fi, Norm ?

186
00:16:05,265 --> 00:16:09,058
Ar fi sup� crem� de m�cri�, Coach.

187
00:16:09,267 --> 00:16:13,643
Urmat de midii coapte
u�or �i cu unt cu tarhon,

188
00:16:13,851 --> 00:16:18,354
continuat de un tort de zmeur� fin�.

189
00:16:20,105 --> 00:16:24,356
Delicatesele fine sunt una
dintre marile pl�ceri ale vie�ii.

190
00:16:24,564 --> 00:16:26,565
Absolut !

191
00:16:28,316 --> 00:16:31,442
Coach, care-i data de expirare la astea ?

192
00:16:38,946 --> 00:16:40,947
Ieri.

193
00:16:42,281 --> 00:16:44,282
Ar trebui s� ne gr�bim.

194
00:16:56,495 --> 00:16:59,163
Sam, ce zici de ni�te b�uturi, s� spele cina ?

195
00:17:01,914 --> 00:17:05,332
A, englez� ! �mi pare r�u.
Ce ai �ntrebat ?

196
00:17:05,541 --> 00:17:08,209
B�uturi ?
Bine. Ce dori�i ?

197
00:17:08,418 --> 00:17:11,544
Un cognac.
Deux.

198
00:17:18,005 --> 00:17:20,923
Cum a fost cina, Sammy ?
Bun�, Sam.

199
00:17:21,131 --> 00:17:23,132
Grozav.
Cin� grozav�.

200
00:17:23,340 --> 00:17:27,050
M�ncarea a fost fran�uzeasc�,
conversa�ia a fost �n greac�.

201
00:17:27,259 --> 00:17:31,678
Toat� sear� am dat din cap ca idiotul.

202
00:17:31,885 --> 00:17:33,886
�n�eleg.

203
00:17:36,722 --> 00:17:40,889
Hei, Sammy ! Ce ar fi s� arunci trampa aia afar� ?

204
00:17:41,098 --> 00:17:46,183
Nu, Diane ar fi furioas�.
Eu vorbeam de Diane.

205
00:17:46,392 --> 00:17:49,643
Nu i-a� l�sa �mpreun�
pe cei doi, �n locul t�u.

206
00:17:51,769 --> 00:17:55,438
Nu am mai v�zut-o pe Diane
distr�ndu-se de mult a�a.

207
00:17:55,646 --> 00:17:57,647
Mul�umesc, prieteni !

208
00:17:58,814 --> 00:18:02,649
Singura dovad� verificabil�
a existen�ei supreme fiind...

209
00:18:02,858 --> 00:18:06,151
Despre ce vorbim ?
Vorbim despre Dumnezeu.

210
00:18:06,359 --> 00:18:08,819
Singura dovad� verificabil�
a existen�ei supreme...

211
00:18:09,027 --> 00:18:11,111
Ce-i cu Dumnezeu ?

212
00:18:12,695 --> 00:18:16,739
E destul de �ncurcat.
�mi place �ncurcat. Bag� mare !

213
00:18:16,905 --> 00:18:20,657
Sumner tocmai spunea c� misticii
antici �l percepeau pe Dumnezeu

214
00:18:20,866 --> 00:18:24,033
f�r� a-l asocia cu dovezi palpabile.

215
00:18:27,160 --> 00:18:29,828
�tii ce zic eu �n privin�a asta ?
Ce zici, Sam ?

216
00:18:37,123 --> 00:18:40,249
Care-i problema ta ?
N-am nici o... Ba da, am o problem�.

217
00:18:40,458 --> 00:18:45,626
Problema e c� nu am luat parte la
nici o conversa�ie �n seara asta.

218
00:18:45,835 --> 00:18:49,712
Am �ncercat de c�teva ori s� te
includ �n conversa�ia noastr�.

219
00:18:49,920 --> 00:18:55,173
Nu cred c� �Furculi�� gre�it� !� �i �Nu
scuipa aia !� e conversa�ia pe care o voiam.

220
00:18:58,048 --> 00:19:04,301
Nu e�ti corect. �i-am cerut opinia
de c�teva ori �i nu ne-ai dat-o ?

221
00:19:04,509 --> 00:19:08,470
�tii de ce ?
Pentru c� nu am avut vreuna.

222
00:19:08,678 --> 00:19:10,679
Ce zici de asta ?

223
00:19:10,888 --> 00:19:14,598
Sau poate aveam, dar nu
vroiam s� o irosesc pe voi.

224
00:19:16,515 --> 00:19:20,391
Ce �ncerci s� spui ?
Nu �ncerc s� spun nimic.

225
00:19:20,600 --> 00:19:23,477
O spun. Noi doi suntem o glum�, Diane.

226
00:19:23,685 --> 00:19:26,686
Tu cu Sumner sunte�i tot o
glum�, dar m�car e aceea�i glum�.

227
00:19:26,895 --> 00:19:30,146
Sunt prost �n multe privin�e,
dar �tiu �i eu c�teva lucruri.

228
00:19:30,354 --> 00:19:34,356
�tiu c�nd doi oameni nu se
potrivesc �i noi doi nu ne potrivim.

229
00:19:34,564 --> 00:19:36,940
Sam, stai jos.
Creezi agita�ie.

230
00:19:37,149 --> 00:19:40,734
Haide�i ! �tiu ce s-a petrecut toat� seara.

231
00:19:40,942 --> 00:19:44,194
Chestia c� Barbara e
bolnav� e numai o prostie.

232
00:19:44,402 --> 00:19:49,487
Ai aranjat totul ca s�-�i recape�i marele
fan �napoi. Ai reu�it ! �ine-o tot a�a, b�iete !

233
00:19:50,696 --> 00:19:54,740
Sam, te rog !
Acum v�d totul foarte clar.

234
00:19:54,948 --> 00:19:59,909
Ai fost o minge recuperat�. Erai
doar aruncat� �ntre mine �i doctori.

235
00:20:00,117 --> 00:20:04,911
A venit dup� tine. Du-te cu el.
��i mai spun ceva.

236
00:20:05,119 --> 00:20:08,954
C�nd o s� mai citesc �R�zboi �i pace� �nc� o dat�
�n cinci zile ca s� impresionez o femeiu�c�,

237
00:20:09,163 --> 00:20:11,748
va fi o zi rece �n Minsk.

238
00:20:25,045 --> 00:20:30,547
Trebuie s�-mi cer scuze �n locul lui
Sam pentru c� a f�cut acuza�ii nefondate.

239
00:20:31,756 --> 00:20:35,007
E mai iste� dec�t am crezut.

240
00:20:37,967 --> 00:20:39,968
Ce ai spus ?

241
00:20:40,177 --> 00:20:43,261
Am vrut s� v�d, dac� a mai r�mas o sc�nteie

242
00:20:43,470 --> 00:20:47,055
din acel foc m�re� ce a
traversat cerul ca un meteor.

243
00:20:49,097 --> 00:20:52,515
Barbara �i cu mine am luat-o pe
c�i separate de c�teva s�pt�m�ni.

244
00:20:54,350 --> 00:20:57,059
Sam avea dreptate.
Las�-l pe Sam.

245
00:20:57,267 --> 00:21:01,061
Nu e evident dup� seara asta
c� noi doi avem ceva special ?

246
00:21:01,269 --> 00:21:03,978
Diane, poate nu sunt eu perfect.

247
00:21:04,187 --> 00:21:06,938
Totu�i s-ar putea s� fiu.

248
00:21:07,146 --> 00:21:13,733
�ntrebarea e dac� po�i s� spui sincer c�
apar�ii mai mult lumii lui dec�t lumii mele ?

249
00:21:29,323 --> 00:21:33,699
Ai venit s�-mi spui la revedere
sau cum spun francezii: ta-ta ?

250
00:21:36,993 --> 00:21:41,411
Nu, am venit s�-�i
spun c� Sumner a plecat.

251
00:21:43,371 --> 00:21:46,789
Da ?
Carla l-a aruncat afar�.

252
00:21:48,539 --> 00:21:51,374
Nu-mi spune c� m-ai ales
pe mine �n locul lui.

253
00:21:51,582 --> 00:21:54,042
Am dat cu banul.

254
00:21:54,250 --> 00:21:57,960
Ai l�sat un ban s� hot�rasc� ?
Da.

255
00:21:58,169 --> 00:22:01,712
I-am zis lui Sumner c� dac� iese cap, vin la tine,

256
00:22:01,921 --> 00:22:04,838
dac� iese pajur�, alerg la tine.

257
00:22:07,340 --> 00:22:11,883
E�ti nebun�, �tii ?
Adic� individul e genial.

258
00:22:12,092 --> 00:22:16,343
E �armant, sofisticat.
Da ? Dac� te gr�be�ti, po�i s�-l iei.

259
00:22:23,222 --> 00:22:27,681
De ce m-ai ales pe mine ?
Tu ai citit �R�zboi �i pace�.

260
00:22:28,765 --> 00:22:31,809
�i el.
Tu ai f�cut-o pentru mine.

261
00:22:33,434 --> 00:22:35,602
Cred c� a fost mai greu pentru tine.

262
00:22:38,019 --> 00:22:40,020
Spune-i inspira�ie.

263
00:22:41,813 --> 00:22:44,272
Nu e ca o zi pe plaj�.

264
00:22:49,900 --> 00:22:54,402
Un lucru e mai romantic dec�t s�
cite�ti �R�zboi �i pace� pentru mine.

265
00:22:54,610 --> 00:22:57,986
Care ?
S�-mi cite�ti mie din �R�zboi �i pace�.

266
00:22:58,195 --> 00:23:00,196
Da ?

267
00:23:00,404 --> 00:23:04,156
Se �nt�mpl� s� am o copie aici.

268
00:23:04,364 --> 00:23:08,282
St�m aici jos �i eu am s�-�i citesc.

269
00:23:09,534 --> 00:23:11,534
Ce dr�gu� !

270
00:23:11,742 --> 00:23:13,743
Hai c� �ncep !

271
00:23:14,786 --> 00:23:17,245
Hai s� vedem filmul.

272
00:23:18,412 --> 00:23:24,414
Au f�cut filmul ? Unde e Cliff ?
�l omor !

273
00:24:00,889 --> 00:24:04,148
Traducerea: tinikabv
