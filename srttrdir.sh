#!/usr/bin/env bash

modify_subtitle() {
  local directory="$1"   

  for file in $(find "$directory"); do
    if [ ! -f "$file" ]; then 
      continue
    fi

    if [[ "$file" =~ .srt$ && ! "$file" =~ .mod.srt$ ]]; then
      srttr "$file" 
    fi

    # if [[ "$file" =~ (.*).mkv$ ]]; then
    #   filename="${BASH_REMATCH[1]}"
    #   if [ -f "$filename.mkv" -a -f "$filename.mod.srt" ]; then
    #     mkvmerge -o "$filename.mod.mkv" "$filename.mkv" "$filename.mod.srt"
    #   fi
    # fi
  done
}

main() {
  if [ "$#" -lt 1 ]; then
    printf "Usage: %s <directory>\n" "${BASH_SOURCE[0]}"
    return 1
  fi

  local directory="$1"
  modify_subtitle "$directory" 
}

main "$@"
